# 自动编译less  

nodejs 监视less文件，发现变化时自动编译成css

#### config.json 配置文件解释
* **interval** int - 默认值500，监视文件的扫描间隔时间，单位毫秒
* **log** string - 日志输出格式
	* full - 默认值，输出监视文件的完整路径 
	* filename - 只输出文件名
* **lesss** array - 需要编译的less文件路径，默认输出到相同路径下，相同名称的css文件，后期再提供修改制定输出目录

先执行 `npm install` 安装依赖 less

执行 `node index.js`